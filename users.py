#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os
import datetime
import random
import Cookie

import storage

class User(object):
	"""Current user"""

	def __init__(self, form):
		self.name = u''
		self.auth = False
		self.message = u''

		store = storage.Storage()

		cookie = Cookie.SimpleCookie()

		if "name" in form:
			self.name = unicode(form["name"].value, 'utf-8')

		# If user has entered correct password, authenticate and create cookie
		if "password" in form and form["password"].value == store.getPassword():
			self.auth = True
			if "remember" in form:
				expires = datetime.datetime.utcnow() + datetime.timedelta(days=30)
				sessionstring = str(random.getrandbits(64))
				cookie['session'] = sessionstring
				cookie['session']['expires'] = expires.strftime("%a, %d %b %Y %H:%M:%S GMT")
				print cookie
				store.saveSession(sessionstring, self.name, expires)

		# Authenticate user based on cookie
		cookie_string = os.environ.get('HTTP_COOKIE')
		if cookie_string:
			cookie.load(cookie_string)
			if 'session' in cookie:
				rows = store.getSession(cookie['session'].value)
				if len(rows) > 0:
					self.auth = True
					self.name = rows[0][1]