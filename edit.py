#!/usr/bin/python
# -*- coding: UTF-8 -*-

import cgi
from jinja2 import Environment, FileSystemLoader

import storage
import users

# Print debug info
import cgitb
cgitb.enable()

# Jinja2
env = Environment(loader=FileSystemLoader('templates/'))

# Setup
store = storage.Storage()
correctPassword = store.getPassword()
form = cgi.FieldStorage()
user = users.User(form)

# Page starts here
print "Content-type: text/html\n"

print env.get_template('head.html').render()

if "id" in form:
	rows = store.getPost(form["id"].value)

for row in rows:
	user.name = row[1]
	user.message = row[2]

# Store any posted edit
if "edit" in form:
	if user.auth:
		store.savePostEdit(unicode(form["edit"].value, 'utf-8'), form["id"].value)
		print '<div class="alert alert-success">Success, your edit was saved!</div>'
		user.message = unicode(form["edit"].value, 'utf-8')

print env.get_template('form.html').render(user=user, textarea='edit').encode('utf-8')

print env.get_template('foot.html').render()