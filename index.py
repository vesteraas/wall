#!/usr/bin/python
# -*- coding: UTF-8 -*-

import cgi
import markdown
from jinja2 import Environment, FileSystemLoader

import storage
import users
from functions import pretty_date

# Print debug info
import cgitb
cgitb.enable()

# Jinja2
env = Environment(loader=FileSystemLoader('templates/'))

# Setup
store = storage.Storage()
correctPassword = store.getPassword()
form = cgi.FieldStorage()
user = users.User(form)

# Page starts here
print "Content-type: text/html\n"

print env.get_template('head.html').render()

if "password" in form and form["password"].value != correctPassword:
	print '<div class="alert alert-danger">Oh snap! Wrong password.</div>'

# Store any posted message
if "message" in form:
	if user.auth:
		store.savePost(user.name, unicode(form["message"].value, 'utf-8'))
		print '<div class="alert alert-success">Success, your post was added!</div>'
	else:
		user.message = unicode(form["message"].value, 'utf-8')

# Load one entry, or all if none is specified
if "id" in form:
	rows = store.getPost(form["id"].value)
else:
	rows = store.getPosts()
	print env.get_template('form.html').render(user=user, textarea='message').encode('utf-8')

# Print entries
for row in rows:
	print env.get_template('post.html').render(
		id=row[0],
		name=row[1],
		time=pretty_date(row[3]),
		content=markdown.markdown(row[2], ['fenced_code', 'mathjax'])
		).encode('utf-8')

# If single post page load comments section
if "id" in form:
	print '<h2>Comments</h2>'
	rows = store.getComments(form["id"].value)
	if "comment" in form:
		if user.auth:
			store.saveComment(user.name, unicode(form["comment"].value, 'utf-8'), form["id"].value)
			print '<div class="alert alert-success">Success, your comment was added!</div>'
		else:
			user.message = unicode(form["comment"].value, 'utf-8')
	for row in rows:
		print env.get_template('post.html').render(
			id=row[0],
			name=row[1],
			time=pretty_date(row[3]),
			content=markdown.markdown(row[2], ['fenced_code', 'mathjax'])
			).encode('utf-8')
	print env.get_template('form.html').render(user=user, textarea='comment').encode('utf-8')

print env.get_template('foot.html').render()
