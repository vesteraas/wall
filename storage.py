#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sqlite3
import datetime
from jinja2 import Environment, FileSystemLoader

# Jinja2
env = Environment(loader=FileSystemLoader('templates/'))

class Storage(object):
	"""
	Abstraction over sqlite db

	Should perhaps be a Singleton?
	"""

	correctPassword = None

	def __init__(self):
		self.db = sqlite3.connect('data.db', detect_types = sqlite3.PARSE_COLNAMES)
		self.cursor = self.db.cursor()

		self.cursor.execute("""CREATE TABLE IF NOT EXISTS Password(
			Password TEXT
		);""")

		self.cursor.execute("""CREATE TABLE IF NOT EXISTS Posts(
		    ID INTEGER PRIMARY KEY,
		    Name TEXT,
		    Text TEXT,
		    Timestamp timestamp
		);""")

		self.cursor.execute("""CREATE TABLE IF NOT EXISTS Comments(
		    ID INTEGER PRIMARY KEY,
		    PostID INTEGER,
		    Name TEXT,
		    Text TEXT,
		    Timestamp timestamp
		);""")

		self.cursor.execute("""CREATE TABLE IF NOT EXISTS Sessions(
		    Session TEXT,
		    Name TEXT,
		    Expires timestamp
		);""")

	def getComments(self, postID):
		self.cursor.execute('''SELECT ID, Name, Text, Timestamp as "Timestamp [timestamp]"
			FROM Comments
			WHERE PostID = ?
			ORDER BY ID DESC''', (postID,))
		return self.cursor.fetchall()

	def getPosts(self):
		self.cursor.execute('''SELECT ID, Name, Text, Timestamp as "Timestamp [timestamp]"
			FROM Posts
			ORDER BY ID DESC''')
		return self.cursor.fetchall()

	def getPost(self, postID):
		self.cursor.execute('''SELECT ID, Name, Text, Timestamp as "Timestamp [timestamp]"
			FROM Posts
			WHERE ID = ?''', (postID,))
		return self.cursor.fetchall()

	def getSession(self, sessionstring):
		self.cursor.execute('SELECT * FROM Sessions WHERE Session = ?', (sessionstring,))
		return self.cursor.fetchall()

	def savePost(self, name, message):
		self.db.execute('''INSERT INTO Posts(Name, Text, Timestamp)
			VALUES(?, ?, ?)''',
			(name, message, datetime.datetime.now()))
		self.db.commit()

	def saveComment(self, name, message, postID):
		self.db.execute('''INSERT INTO Comments(Name, Text, Timestamp, PostID)
			VALUES(?, ?, ?, ?)''',
			(name, message, datetime.datetime.now(), postID))
		self.db.commit()

	def saveSession(self, sessionstring, name, expires):
		self.db.execute('''INSERT INTO Sessions(Session, Name, Expires)
			VALUES(?, ?, ?)''',
			(sessionstring, name, expires))
		self.db.commit()

	def savePostEdit(self, message, postID):
		self.db.execute('''UPDATE Posts SET Text=? WHERE ID=?''',
			(message, postID))
		self.db.commit()

	def getPassword(self):
		"""Has to be called before page starts, or not at all, which could be a problem..."""
		if self.correctPassword is None:
			# Get password from database, or abort if it does not exist
			self.cursor.execute('''SELECT * FROM Password''')
			passrows = self.cursor.fetchall()
			if len(passrows) > 0:
				self.correctPassword = passrows[0][0]
			else:
				print "Content-type: text/html\n"
				print env.get_template('head.html').render()
				print "<p>Password not set!</p>"
				print "<p>To set up, do this:</p>"
				print """<pre><code>sqlite3 data.db\nsqlite> insert into Password(Password) values("yourpasswordhere");</pre></code>"""
				print env.get_template('foot.html').render()
				exit()
		return self.correctPassword

	def getCursor(self):
		return self.cursor

	def getDB(self):
		return self.db


