#!/usr/bin/python
# -*- coding: UTF-8 -*-

import cgitb
cgitb.enable()

import cgi
import storage

print 'Content-type: text/plain; charset=utf-8\n'

form = cgi.FieldStorage()
store = storage.Storage()

rows = store.getPost(form["id"].value)

for row in rows:
	print row[2].encode('utf-8')


